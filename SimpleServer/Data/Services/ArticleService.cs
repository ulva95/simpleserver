using Microsoft.EntityFrameworkCore;
using SimpleServer.Data.DTOs;
using SimpleServer.Data.Models;

namespace SimpleServer.Data.Services;

public class ArticleService
{
    private EducationContext _context;

    public ArticleService(EducationContext context)
    {
        _context = context;
    }

    public async Task<Article?> AddArticle(ArticleDTO article)
    {
        Article narticle = new Article
        {
            Abstract = article.Abstract,
            Keywords = article.Keywords,
            Title = article.Title
        };
        if (article.AuthorIds?.Length > 0)
        {
            narticle.Authors = _context.Authors.ToList().IntersectBy(article.AuthorIds, author => author.Id).ToList();
        }

        var result = _context.Articles.Add(narticle);
        await _context.SaveChangesAsync();
        return await Task.FromResult(result.Entity);
    }

    public async Task<Article?> GetArticle(int id)
    {
        var result = _context.Articles.Include(a => a.Authors).FirstOrDefault(article => article.Id == id);

        return await Task.FromResult(result);
    }

    public async Task<List<Article>> GetArticles()
    {
        var result = _context.Articles.Include(a =>
                a.Authors)
            .ToList();
        return await Task.FromResult(result);
    }

    public async Task<Article?> UpdateArticle(int id, ArticleDTO nArticle)
    {
        var artcile = await _context.Articles.FirstOrDefaultAsync(ar => ar.Id == id);
        if (artcile != null)
        {
            artcile.Abstract = nArticle.Abstract;
            artcile.Keywords = nArticle.Keywords;
            artcile.Title = nArticle.Title;
            if (nArticle.AuthorIds?.Length > 0)
            {
                artcile.Authors = _context.Authors.ToList().IntersectBy(nArticle.AuthorIds, author => author.Id)
                    .ToList();
            }

            await _context.SaveChangesAsync();
            return artcile;
        }

        return null;
    }

    public async Task<bool> DeleteArticle(int id)
    {
        var article = await _context.Articles.FirstOrDefaultAsync(ar => ar.Id == id);
        if (article != null)
        {
            _context.Articles.Remove(article);
            await _context.SaveChangesAsync();
            return true;
        }

        return false;
    }
}