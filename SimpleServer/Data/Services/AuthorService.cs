using Microsoft.EntityFrameworkCore;
using SimpleServer.Data.DTOs;
using SimpleServer.Data.Models;

namespace SimpleServer.Data.Services;

public class AuthorService
{
    private EducationContext _context;
    public AuthorService(EducationContext context)
    {
        _context = context;
    }

    public async Task<Author?> AddAuthor(AuthorDTO author)
    {
        
        Author nauthor = new Author
        {
            Fullname = author.Fullname,
            Position = author.Position,
            Grade = author.Grade
        };
        if (author.Affiliations.Any())
        {
            var aff = _context.Affiliations.ToList();
            nauthor.Affiliations  = _context.Affiliations.ToList().IntersectBy(author.Affiliations, affiliation => affiliation.Id).ToList();
        }
        var result = _context.Authors.Add(nauthor);
        await _context.SaveChangesAsync();
        return await Task.FromResult(result.Entity);
    }
    public async Task<Author?> GetAuthor(int id)
    {
        
        var result = _context.Authors.Include(a=>a.Affiliations).Include(b=>b.Articles).FirstOrDefault(author => author.Id==id);
        
        return await Task.FromResult(result);
    }
    public async Task<List<Author>> GetAuthors()
    {
        var result = await _context.Authors.Include(a=>a.Affiliations).Include(b=>b.Articles).ToListAsync();
        return await Task.FromResult(result);
    }
    
    public async Task<List<IncompleteAuthorDTO>> GetAuthorsIncomplete()
    {
        var authors = await _context.Authors.ToListAsync();
        List<IncompleteAuthorDTO> result = new List<IncompleteAuthorDTO>();
        authors.ForEach(au=> result.Add(new IncompleteAuthorDTO{Id = au.Id, Fullname = au.Fullname, Position = au.Position}));
        return await Task.FromResult(result);
    }
  
    public async Task<Author?> UpdateAuthor(int id, Author updatedAuthor)
    {
        var author = await _context.Authors.Include(author=>author.Affiliations).Include(b=>b.Articles).FirstOrDefaultAsync(au => au.Id == id);
        if (author != null)
        {
            author.Fullname = updatedAuthor.Fullname;
            author.Position = updatedAuthor.Position;
            author.Grade = updatedAuthor.Grade;
            if (updatedAuthor.Affiliations.Any())
            {
                author.Affiliations  = _context.Affiliations.ToList().IntersectBy(updatedAuthor.Affiliations, affiliation => affiliation).ToList();
            }
            if (updatedAuthor.Articles.Any())
            {
                author.Articles  = _context.Articles.ToList().IntersectBy(updatedAuthor.Articles, article => article).ToList();
            }
            _context.Authors.Update(author);
            _context.Entry(author).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return author;
        }

        return null;
    }
  
    public async Task<bool> DeleteAuthor(int id)
    {
        var author = await _context.Authors.FirstOrDefaultAsync(au => au.Id == id);
        if (author != null)
        {
            _context.Authors.Remove(author);
            await _context.SaveChangesAsync();
            return true;
        }

        return false;
    }
}