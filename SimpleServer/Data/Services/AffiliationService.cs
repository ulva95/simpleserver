using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using NuGet.Packaging.Licenses;
using SimpleServer.Data.DTOs;
using SimpleServer.Data.Models;

namespace SimpleServer.Data.Services;

using SimpleServer.Data;

public class AffiliationService
{
    private EducationContext _context;

    public AffiliationService(EducationContext context)
    {
        _context = context;
    }

    public async Task<Affiliation?> AddAffiliation(AffiliationDTO affiliation)
    {
        Affiliation affil = new Affiliation
        {
            Address = affiliation.Address, Specialization = affiliation.Specialization,
            CompanyName = affiliation.CompanyName
        };
        var result = _context.Affiliations.Add(affil);
        await _context.SaveChangesAsync();
        
        return await Task.FromResult(result.Entity);
    }

    public async Task<Affiliation?> GetAffiliation(int id)
    {
        var result = _context.Affiliations
            .FirstOrDefault(affiliation => affiliation.Id == id);

        return await Task.FromResult(result);
    }

    public async Task<List<Affiliation>> GetAffiliations()
    {
        var result = await _context.Affiliations.ToListAsync();
        return await Task.FromResult(result);
    }


    public async Task<Affiliation?> UpdateAffiliation(int id, AffiliationDTO affil)
    {
        var affiliation = await _context.Affiliations.FirstOrDefaultAsync(af => af.Id == id);
        if (affiliation != null)
        {
            affiliation.Address = affil.Address;
            affiliation.Specialization = affil.Specialization;
            affiliation.CompanyName = affil.CompanyName;
            _context.SaveChanges();
            return affiliation;
        }

        return null;
    }

    public async Task<bool> DeleteAffiliation(int id)
    {
        var affiliation = await _context.Affiliations.FirstOrDefaultAsync(af => af.Id == id);
        if (affiliation != null)
        {
            _context.Affiliations.Remove(affiliation);
            _context.SaveChanges();
            return true;
        }

        return false;
    }
}