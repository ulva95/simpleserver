namespace SimpleServer.Data.DTOs;

public class IncompleteAuthorDTO
{
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string Position { get; set; }
}