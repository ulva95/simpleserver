namespace SimpleServer.Data.DTOs;

public class AffiliationDTO
{
    public string CompanyName { get; set; }
    public string Address { get; set; }
    public string Specialization { get; set; }
}