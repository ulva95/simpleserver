namespace SimpleServer.Data.DTOs;

public class ArticleDTO
{
    public string Title { get; set; }
    public string Abstract { get; set; }
    public string Keywords { get; set; }
    public int[] AuthorIds { get; set; }
        
}