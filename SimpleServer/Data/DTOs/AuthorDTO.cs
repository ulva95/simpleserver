namespace SimpleServer.Data.DTOs;

public class AuthorDTO
{
    public string Fullname { get; set; }
    public string Position { get; set; }
    public string Grade { get; set; }
    public int[] Affiliations { get; set; }
}