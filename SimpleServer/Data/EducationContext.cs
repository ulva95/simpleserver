using Microsoft.EntityFrameworkCore;
using SimpleServer.Data.Models;

namespace SimpleServer.Data;

public class EducationContext : DbContext
{


// public EducationContext(){}
    // public EducationContext(DbContextOptions<EducationContext> options) : base(options)
    // {
    //   
    // }
    

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);
        optionsBuilder.UseNpgsql(@"Host=localhost;Database=education;Username=education;Password=password")
            .UseSnakeCaseNamingConvention()
            .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole())).EnableSensitiveDataLogging();
        
        
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Author>().Property(p => p.Id).ValueGeneratedOnAdd();
        modelBuilder.Entity<Article>().Property(p => p.Id).ValueGeneratedOnAdd();
        modelBuilder.Entity<Affiliation>().Property(p => p.Id).ValueGeneratedOnAdd();
        modelBuilder.Entity<Author>().HasMany(au => au.Affiliations).WithMany(af => af.Authors);
        modelBuilder.Entity<Article>().HasMany(ar => ar.Authors).WithMany(au => au.Articles);
        
    }

    public DbSet<Author> Authors { get; set; }
    public DbSet<Article> Articles { get; set; }
    public DbSet<Affiliation> Affiliations { get; set; }
}