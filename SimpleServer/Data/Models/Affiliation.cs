using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleServer.Data.Models;

public class Affiliation
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column(Order = 1)]
    public int Id { get; set; }
    public string CompanyName { get; set; }
    public string Address { get; set; }
    public string Specialization { get; set; }
    public IEnumerable<Author> Authors { get; set; }
}