using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleServer.Data.Models;

public class Author
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column(Order = 1)]
    public int Id { get; set; }
    public string Fullname { get; set; }
    public string Position { get; set; }
    public string Grade { get; set; }
    public IEnumerable<Affiliation> Affiliations { get; set; }
    public IEnumerable<Article> Articles { get; set; }
}