using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleServer.Data.Models;

public class Article
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column(Order = 1)]
    public int Id { get; set; }
    public string Title { get; set; }
    public string Abstract { get; set; }
    public string Keywords { get; set; }
    public IEnumerable<Author> Authors { get; set; }
}