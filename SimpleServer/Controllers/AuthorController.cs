#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimpleServer.Data;
using SimpleServer.Data.DTOs;
using SimpleServer.Data.Models;
using SimpleServer.Data.Services;

namespace SimpleServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly AuthorService _context;

        public AuthorController(AuthorService context)
        {
            _context = context;
        }

        // GET: api/Author
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Author>>> GetAuthor()
        {
            return await _context.GetAuthors();
        }

        [HttpGet("/incomplete")]
        public async Task<ActionResult<IEnumerable<IncompleteAuthorDTO>>> GetAuthorIncomplete()
        {
            return await _context.GetAuthorsIncomplete();
        }
           
        // GET: api/Author/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Author>> GetAuthor([FromRoute] int id)
        {
            var author = await _context.GetAuthor(id);

            if (author == null)
            {
                return NotFound();
            }

            return author;
        }

        // PUT: api/Author/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Author>> PutAuthor(int id, [FromBody] Author author)
        {
            var result = await _context.UpdateAuthor(id, author);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok(result);
        }

        // POST: api/Author
        [HttpPost]
        public async Task<ActionResult<Author>> PostAuthor([FromBody] AuthorDTO author)
        {
            var result = await _context.AddAuthor(author);
            if (result == null)
            {
                BadRequest();
            }

            return Ok(result);
        }

        // DELETE: api/Author/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAuthor(int id)
        {
            var author = await _context.DeleteAuthor(id);
            if (author)
            {
                return Ok();
            }
            return BadRequest();
        }

    }
}