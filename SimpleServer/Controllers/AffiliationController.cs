#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimpleServer.Data;
using SimpleServer.Data.Models;
using SimpleServer.Data.DTOs;
using SimpleServer.Data.Services;

namespace SimpleServer.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class AffiliationController : ControllerBase
    {
        private readonly AffiliationService _context;

        public AffiliationController(AffiliationService context)
        {
            _context = context;
        }

        // GET: api/Affiliation
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Affiliation>>> GetAffiliations()
        {
            return await _context.GetAffiliations();
        }

        // GET: api/Affiliation/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Affiliation>> GetAffiliation([FromRoute]int id)
        {
            var affiliation = await _context.GetAffiliation(id);
            if (affiliation == null)
            {
                return NotFound();
            }
            return affiliation;
        }

        // PUT: api/Affiliation/5
       
        [HttpPut("{id}")]
        public async Task<ActionResult<Affiliation>> PutAffiliation([FromRoute]int id,[FromBody] AffiliationDTO affil)
        {
            var affiliation = await _context.UpdateAffiliation(id, affil);
            if (affiliation == null)
            {
                return Ok();
            }
            return BadRequest(affiliation);
        }
        

        // POST: api/Affiliation
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Affiliation>> PostAffiliation([FromBody]AffiliationDTO affiliation)
        {
            
        var result =  await _context.AddAffiliation(affiliation);
        if (result == null)
        {
            return BadRequest();
        }

        return Ok(result);
        }

        // DELETE: api/Affiliation/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAffiliation(int id)
        {
            var affiliation = await _context.DeleteAffiliation(id);
            if (affiliation)
            {
                return Ok();
            }
            return NoContent();
        }

    }
}
