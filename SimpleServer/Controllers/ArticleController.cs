#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimpleServer.Data;
using SimpleServer.Data.DTOs;
using SimpleServer.Data.Models;
using SimpleServer.Data.Services;

namespace SimpleServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private readonly ArticleService _context;

        public ArticleController(ArticleService context)
        {
            _context = context;
        }

        // GET: api/Article
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Article>>> GetArticle()
        {
            return await _context.GetArticles();
        }

        // GET: api/Article/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Article>> GetArticle(int id)
        {
            var article = await _context.GetArticle(id);

            if (article == null)
            {
                return NotFound();
            }

            return article;
        }

        // PUT: api/Article/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<ActionResult<Article>> PutArticle([FromRoute]int id, [FromBody]ArticleDTO article)
        {

            var art = await _context.UpdateArticle(id, article);

            if (art == null)
            {
                return BadRequest();
            }

            return Ok(art);
        }

        // POST: api/Article
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Article>> PostArticle([FromBody]ArticleDTO article)
        {
           var result = await  _context.AddArticle(article);

           if (result == null)
           {
              return BadRequest();
           }

           return Ok(result);
        }

        // DELETE: api/Article/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteArticle(int id)
        {
            var article = await _context.DeleteArticle(id);
            if (!article)
            {
                return NotFound();
            }

            return Ok();
        }
        
    }
}
