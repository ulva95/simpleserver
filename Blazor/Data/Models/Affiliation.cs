
namespace Blazor.Data.Models;

public class Affiliation
{

    public int Id { get; set; }
    public string CompanyName { get; set; }
    public string Address { get; set; }
    public string Specialization { get; set; }
    public IEnumerable<Author> Authors { get; set; }
}