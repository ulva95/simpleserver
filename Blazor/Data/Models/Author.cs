
using Blazor.Data.Models;

namespace Blazor.Data.Models;

public class Author
{
    public int Id { get; set; }
    public string Fullname { get; set; }
    public string Position { get; set; }
    public string Grade { get; set; }
    public IEnumerable<Affiliation> Affiliations { get; set; }
    public IEnumerable<Article> Articles { get; set; }
}