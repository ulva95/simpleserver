
using Blazor.Data.Models;

namespace Blazor.Data.Models;

public class Article
{

    public int Id { get; set; }
    public string Title { get; set; }
    public string Abstract { get; set; }
    public string Keywords { get; set; }
    public IEnumerable<Author> Authors { get; set; }
}